msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-13 14:21-0300\n"
"PO-Revision-Date: 2021-03-13 14:21-0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.3.2\n"
"Language: zh-TW\n"

#: HTML/index.tpl.html%2Bhtml[lang]:2-1
msgid "en-US"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.head.title:4-7
msgctxt "HTML/index.tpl.html+html.head.title:4-7"
msgid "Welcome to CentOS Stream 9"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.header.div.div.div:18-15
msgid "<strong>CentOS</strong> Stream 9"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.h1:24-10
msgctxt "HTML/index.tpl.html+html.body.main.h1:24-10"
msgid "Welcome to CentOS Stream 9"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.p:25-10
msgid ""
"CentOS Stream is a continuously delivered distro that tracks just ahead of "
"Red Hat Enterprise Linux (RHEL) development, positioned as a midstream "
"between Fedora Linux and RHEL. For anyone interested in participating and "
"collaborating in the RHEL ecosystem, CentOS Stream is your reliable platform "
"for innovation."
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.p:26-10
msgid ""
"CentOS Stream is a developer-forward distribution that aims to help community "
"members, Red Hat partners and others take full advantage of open source "
"innovation within a more stable and predictable Linux ecosystem. Its content "
"is what Red Hat intends to be in the next update of a stable RHEL release. It "
"is free for anyone to download, use, study, modify, and redistribute (with "
"the exception of the CentOS trademarks, which are owned by Red Hat). CentOS "
"Stream is a distribution that community members can use to take advantage of "
"a stable ABI/API for development and testing, while still seeing some updates "
"on an accelerated basis."
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.h3:30-19
msgid "About"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:32-22
msgid "About CentOS"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:33-22
msgid "Frequently Asked Questions (FAQs)"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:34-22
msgid "Special Interest Groups (SIGs)"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:35-22
msgid "CentOS Variants"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:36-22
msgid "Governance"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.h3:40-19
msgid "Community"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:42-21
msgid "Contribute"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:43-21
msgid "Forums"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:44-21
msgid "Mailing Lists"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:45-21
msgid "IRC"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:46-21
msgid "Calendar &amp; IRC Meeting List"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:47-21
msgid "Planet"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:48-21
msgid "Submit Bug"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.h3:52-18
msgid "Documentation"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:54-21
msgid "Wiki"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:55-21
msgid "Manuals"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.main.div.div.div.ul.li:56-21
msgid "GPG Key Info"
msgstr ""

#: HTML/index.tpl.html%2Bhtml.body.footer.div:63-11
msgid ""
"Copyright © 2021 The CentOS Project | <a href=\"/legal\">Legal</a> | <a href="
"\"/privacy\">Privacy</a>"
msgstr ""
