Summary: Browser default start page for CentOS
Name: centos-indexhtml
Version: 9.5
Release: 1%{?dist}
Source: %{name}-%{version}.tar.gz
License: Distributable
Group: Documentation
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: translate-toolkit
BuildRequires: yuicompressor
BuildRequires: tidy
Obsoletes: indexhtml <= 2:5-1
Obsoletes: redhat-indexhtml
Provides: redhat-indexhtml

%description
The indexhtml package contains the welcome page shown by your Web browser,
which you'll see after you've successfully installed CentOS Stream.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_defaultdocdir}/HTML
for i in PO/??-??.po;do
  make build lang=$(basename ${i%%.*}) DESTDIR=$RPM_BUILD_ROOT/%{_defaultdocdir}/HTML/
done

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_defaultdocdir}/HTML/*

%changelog
* Fri Apr 16 2021 Alain Reguera Delgado <areguera@centosproject.org> 9.5-1
- Update stylesheet based on resent website updates
  (areguera@centosproject.org)

* Sat Apr 03 2021 Alain Reguera Delgado <areguera@centosproject.org> 9.4-1
- Redesign based on website visual styles (areguera@centosproject.org)

* Sun Mar 14 2021 Alain Reguera Delgado <areguera@centosproject.org> 9.3-1
- Add .gitlab-ci.yml to automate rpm builds (areguera@centosproject.org)
- Remove unused file. (areguera@centosproject.org)

* Sun Mar 14 2021 Alain Reguera Delgado <areguera@centosproject.org> 9.2-1
- new package built with tito

* Sat Mar 13 2021 Alain Reguera Delgado <areguera@centosproject.org> 9.1-0
- Update page design and content for CentOS Stream 9

* Wed Mar  3 2021 Johnny Hughes <johnny@centos.org> 9.0-0
- Initial Build for CentOS Stream 9

* Sun Apr 28 2019 Alain Reguera Delgado <alain.reguera@gmail.com> 8.0-0
- Roll in CentOS artwork based on CentOS Artistic Motif Sketch 4

* Sun Jun 29 2014 Johnny Hughes <johnny@centos.org> 7-9.el7.centos
- Add en-US directory

* Fri May 16 2014 Johnny Hughes <johnny@centos.org> 7-8.el7.centos
- Roll in CentOS Branding
