###############################################################################
#
#  CentOS web browser default page
#  Copyright (C) 2021 Alain Reguera Delgado
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <https://www.gnu.org/licenses/>.
#
###############################################################################

#==============================================================================
# Configuration
#==============================================================================
lang = en-US
localedir = PO
htmldir = HTML
release = 9.0
DESTDIR = centos-indexhtml-$(release)
tidy = /usr/bin/tidy -config tidy.conf

locale: $(localedir)/$(lang).po

build: $(DESTDIR)/common/css $(DESTDIR)/common/webfonts $(DESTDIR)/common/images $(DESTDIR)/index.html $(DESTDIR)/$(lang)/index.html

#==============================================================================
# Requirements
#==============================================================================
/usr/bin/html2po:
	sudo dnf -y install translate-toolkit

/usr/bin/po2html:
	sudo dnf -y install translate-toolkit

/usr/bin/yuicompressor:
	sudo dnf -y install yuicompressor

/usr/bin/tidy:
	sudo dnf -y install tidy

#==============================================================================
# Localization
#==============================================================================
$(localedir)/$(lang).pot: /usr/bin/html2po $(htmldir)/index.tpl.html
	html2po -P --input $(htmldir)/index.tpl.html --output /tmp/$(lang).pot
	if [[ ! -f $(localedir)/$(lang).pot ]];then \
		msginit -i /tmp/$(lang).pot -o $(localedir)/$(lang).pot -l $(lang) --no-translator --width=80; \
	else \
		msgmerge -U $(localedir)/$(lang).pot /tmp/$(lang).pot; \
	fi
	rm /tmp/$(lang).pot

$(localedir)/$(lang).po: $(localedir)/$(lang).pot
	if [[ ! -f $(localedir)/$(lang).po ]];then \
		msginit -i $(localedir)/$(lang).pot -o $(localedir)/$(lang).po -l $(lang) --no-translator --width=80; \
	else \
		msgmerge -U $(localedir)/$(lang).po $(localedir)/$(lang).pot; \
	fi

#==============================================================================
# Package
#==============================================================================
$(DESTDIR):
	mkdir -p $(DESTDIR)

$(DESTDIR)/index.html: /usr/bin/tidy $(DESTDIR) $(htmldir)/index.html tidy.conf 
	$(tidy) -o $(DESTDIR)/index.html $(htmldir)/index.html

$(DESTDIR)/$(lang):
	mkdir -p $(DESTDIR)/$(lang)

$(DESTDIR)/common/css: /usr/bin/yuicompressor
	mkdir -p $(DESTDIR)/common/css
	rsync -a --delete $(htmldir)/common/css/*.min.css $(DESTDIR)/common/css/
	yuicompressor $(htmldir)/common/css/bootstrap-centos.indexhtml.css -o $(DESTDIR)/common/css/bootstrap-centos.indexhtml.min.css

$(DESTDIR)/common/images:
	mkdir -p $(DESTDIR)/common/images
	rsync -a --delete $(htmldir)/common/images/ $(DESTDIR)/common/images/

$(DESTDIR)/common/webfonts:
	mkdir -p $(DESTDIR)/common/webfonts
	rsync -a --delete $(htmldir)/common/webfonts/ $(DESTDIR)/common/webfonts/

$(DESTDIR)/$(lang)/index.html: /usr/bin/po2html /usr/bin/tidy $(localedir)/$(lang).po $(htmldir)/index.tpl.html tidy.conf $(DESTDIR)/$(lang)
	po2html -i $(localedir)/$(lang).po -t $(htmldir)/index.tpl.html -o $(DESTDIR)/$(lang)/index.html 
	$(tidy) -m $(DESTDIR)/$(lang)/index.html

#==============================================================================
# Cleanup
#==============================================================================
clean:
	rm -r $(DESTDIR)
